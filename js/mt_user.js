/**
 * @file
 */
(function ($) {
  Drupal.behaviors.mt_user_script = {
    attach: function (context, settings) {
			if ($('#mt-user-filters-form').length > 0) {
				$('#mt-user-filters-form').on('click', '.btn-mt-user-export', function() {
					var id = $('#edit-user-filter-id').val();
					var name = $('#edit-user-filter-name').val();
					var status = $('#edit-user-active').val();
					if (status == 'All') status = '';
					var href = $('.btn-mt-user-export').attr('href');
					href = href+'?id='+id+'&name='+name+'&status='+status;
					$('.btn-mt-user-export').attr('href', href);
				});
			}
    }
  };
})(jQuery);