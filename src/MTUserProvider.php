<?php

/**
 * @file
 * Contains \Drupal\mt_user/src/Controller/MTUserProvider.php
 */

namespace Drupal\mt_user;

/**
 * Description of MTUserProvider
 *
 * @author marco.torres
 */
class MTUserProvider {

	/*
	* process data register custom User
	*/
	public function processDataUser($values) {
	    $output = [
	      'name' => $values['user_name'],
	      'status' => !empty($values['user_active']) ? TRUE : FALSE
	    ];
	    return $output;
	}

	/*
	* get table registers custom User
	*/
	public function getTableUser($header, $num_per_page = 20, $filters = NULL) {
		try {
			$db = \Drupal\Core\Database\Database::getConnection();
		    $query = $db->select('mt_user','u');
			$query->fields('u',
				[
					'id',
					'name',
					'status',
				]
			);

			if (!empty($filters)) {
				if (!empty($filters['id'])) $query->condition('id', $filters['id']);
				if (!empty($filters['name'])) $query->condition('name', '%'.$filters['name'].'%', 'LIKE');
				if (isset($filters['status']) && $filters['status'] != NULL) $query->condition('status', $filters['status']);
			}

		    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
		                        ->orderByHeader($header);
		    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
		                        ->limit($num_per_page);
		    $result = $pager->execute();
    		$table = $result->fetchAll();
			if (!empty($table)) {
				return [
					'table' => $table,
					'pager' => $pager
				];
			}
			return FALSE;
		}
		catch (\Exception $e) {
      		return $e->getMessage();
		}

	}

	/*
	* get register custom User
	*/
	public function getUser($id) {
		try {
    		$db = \Drupal\Core\Database\Database::getConnection();
			$query = $db->select('mt_user','u');
			$query->fields('u',
				[
					'id',
					'name',
					'status',
				]
			);
			$query->condition('id', $id);
			$result = $query->execute();
    		$output = $result->fetchObject();
			return (!empty($output)) ? $output : FALSE;
		}
		catch (Exception $e) {
			return FALSE;
		}
	}

	/*
	* get register custom last User
	*/
	public function getLastUser() {
		try {
    		$db = \Drupal\Core\Database\Database::getConnection();
			$query = $db->select('mt_user','u');
			$query->fields('u',
				[
					'id'
				]
			);
			$query->orderBy('id', 'DESC');
			$result = $query->execute();
    		$output = $result->fetchField();
			return (!empty($output)) ? $output : FALSE;
		}
		catch (Exception $e) {
			return FALSE;
		}
	}

	/*
	* get register custom User with filters
	*/
	public function getAPIUser($params) {

        if (!empty($params['id'])) {
        	$id = is_array($params['id']) ? $params['id'] : explode(',', $params['id']);
        }
		if (empty($params['all'])) {
			$limit = !empty($params['limit']) ? $params['limit'] : 50;
			$offset = !empty($params['offset']) ? $params['offset'] : 0;
		}

		try {
    		$db = \Drupal\Core\Database\Database::getConnection();
			$query = $db->select('mt_user','u');
			$query->fields('u',
				[
					'id',
					'name',
					'status',
					'created',
					'modified',
				]
			);
			if (!empty($id)) $query->condition('u.id', $id, 'IN');
			if (!empty($params['name'])) $query->condition('u.name', '%'.$params['name'].'%', 'LIKE');
			if (isset($params['status']) && $params['status'] !== NULL) $query->condition('u.status', $params['status']);
			if (empty($params['all'])) $query->range($offset, $limit);
			$output = $query->execute()->fetchAll();
			if (!empty($output)) return $output;
			return FALSE;
		}
		catch (Exception $e) {
			return FALSE;
		}
		return FALSE;
	}

	/*
	* create register custom User
	*/
	public function createUser($data) {
		try {
			$date_time = time();
    		$db = \Drupal\Core\Database\Database::getConnection();
			$query = $db->insert('mt_user');
			$query->fields(
				[
					'name' => $data['name'],
					'status' => !empty($data['status']) ? 1 : 0,
					'created' => $date_time,
					'modified' => $date_time,
				]
			);
			$query->execute();
			return TRUE;
		}
		catch (Exception $e) {
			return FALSE;
		}
	}

	/*
	* edit register custom User
	*/
	public function editUser($id, $data) {
		try {
			$date_time = time();
    		$db = \Drupal\Core\Database\Database::getConnection();
			$query = $db->update('mt_user');
			$query->fields(
				[
					'name' => $data['name'],
					'status' => !empty($data['status']) ? 1 : 0,
					'modified' => $date_time,
				]
			);
			$query->condition('id', $id);
			$query->execute();
			return TRUE;
		}
		catch (Exception $e) {
			return FALSE;
		}
	}

	/*
	* delete register custom User
	*/
	public function deleteUser($id) {
		try {
    		$db = \Drupal\Core\Database\Database::getConnection();
			$query = $db->delete('mt_user');
			$query->condition('id', $id);
			$query->execute();
			return TRUE;
		}
		catch (Exception $e) {
			return FALSE;
		}
	}
}

