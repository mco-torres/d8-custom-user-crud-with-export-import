<?php

namespace Drupal\mt_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Path\AliasStorageInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\mt_user\MTUserProvider;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class MTUserController.
 *
 * @package Drupal\mt_user\Controller
 */
class MTUserController extends ControllerBase {

  public function list_user(Request $request) {

    $filters = $request->query->get('search');
    $build['mt_user_filters'] = $this->formBuilder()->getForm('Drupal\mt_user\Form\FiltersForm', $filters);
    $destination = $this->getDestinationArray();
    $class = new MTUserProvider();

    $rows = [];
    $header = [
      0 => ['data' => $this->t('Id'), 'field' => 'id', 'sort' => 'desc'],
      1 => ['data' => $this->t('Name'), 'field' => 'url_origin'],
      2 => ['data' => $this->t('Status'), 'field' => 'status'],
      3 => $this->t('Operations')
    ];

    $data = $class->getTableUser($header, 20, $filters);
    if (!empty($data)) {
      foreach ($data['table'] as $row) {
        $rows[] = [
          'data' => [
            'id' => $row->id,
            'name' => $row->name,
            'status' => !empty($row->status) ? 'Yes' : 'No',
            'operations' => [
              'data' => [
                '#type' => 'operations',
                '#links' => [
                  'edit' => [
                    'title' => $this->t('Edit'),
                    'url' => Url::fromRoute('mt_user.admin.edit', ['id' => $row->id], ['query' => $destination]),
                  ],
                  'delete' => [
                    'title' => $this->t('Delete'),
                    'url' => Url::fromRoute('mt_user.admin.drop', ['id' => $row->id], ['query' => $destination]),
                  ]
                ]
              ]
            ]
          ]
        ];
      }
    }

    $build['mt_user_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No records found. <a href=":link">Add custom User</a>.', array(':link' => $this->url('mt_user.admin.add'))),
    ];
    $build['mt_user_pager'] = array('#type' => 'pager');

    return $build;
  }

  public function export_user(Request $request) {

    // Variables
    if (!empty($_GET['id'])) $params['id'] = $_GET['id'];
    if (!empty($_GET['name'])) $params['name'] = $_GET['name'];
    if (isset($_GET['status']) && $_GET['status'] != NULL) $params['status'] = $_GET['status'];

    // Require
    require 'libraries/excel/PHPExcel.php';
    require 'libraries/excel/PHPExcel/IOFactory.php';
    require 'libraries/excel/PHPExcel/Writer/Excel2007.php';

    // Filename and Date
    $filename = 'mt_user_report_';
    $filename .= date('Y_m_d_H_i', time());

    // Header
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename='.$filename.'.xls');
    header('Pragma: no-cache');
    header('Expires: 0');

    // create new PHPExcel object
    $objPHPExcel = new \PHPExcel;

    //Set properties
    $objPHPExcel
      ->getProperties()
      ->setCreator('Marco Torres')
      ->setLastModifiedBy('Marco Torres')
      ->setTitle("PHPExcel Users")
      ->setLastModifiedBy('Marco Torres')
      ->setDescription('Report content Excel file')
      ->setSubject('PHP Excel content by filter')
      ->setKeywords('excel php office phpexcel')
      ->setCategory('programming');

    // set default font
    $objPHPExcel->getDefaultStyle()->getFont()->setName('Ubuntu');

    // set default font size
    $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);

    // create the writer
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    /*** Users List ***/

    $objSheet = $objPHPExcel->getActiveSheet();

    // rename the sheet
    $nameSheet = t('Report').' Users';
    $objSheet->setTitle($nameSheet);

    $params['all'] = 1;
    $objSheet = $this->get_export_user_list($objSheet, $params);

    $objWriter->save('php://output');
    exit();
  }

  public function export_user_template(Request $request) {

    // Require
    require 'libraries/excel/PHPExcel.php';
    require 'libraries/excel/PHPExcel/IOFactory.php';
    require 'libraries/excel/PHPExcel/Writer/Excel2007.php';

    // Filename and Date
    $filename = 'mt_user_report_template';

    // Header
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename='.$filename.'.xls');
    header('Pragma: no-cache');
    header('Expires: 0');

    // create new PHPExcel object
    $objPHPExcel = new \PHPExcel;

    //Set properties
    $objPHPExcel
      ->getProperties()
      ->setCreator('GLR')
      ->setLastModifiedBy('GLR')
      ->setTitle("PHPExcel audit")
      ->setLastModifiedBy('GLR')
      ->setDescription('Report content Excel file')
      ->setSubject('PHP Excel content by filter')
      ->setKeywords('excel php office phpexcel')
      ->setCategory('programming');

    // set default font
    $objPHPExcel->getDefaultStyle()->getFont()->setName('Ubuntu');

    // set default font size
    $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);

    // create the writer
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    /*** Users List Empty ***/

    $objSheet = $objPHPExcel->getActiveSheet();

    // rename the sheet
    $nameSheet = t('Report').' Users';
    $objSheet->setTitle($nameSheet);

    $objSheet = $this->get_export_user_template($objSheet);

    $objWriter->save('php://output');
    exit();
  }

  public function add_user() {
    $form = $this->formBuilder()->getForm('Drupal\mt_user\Form\CreateForm');
    return [
      '#theme' => 'theme_mt_user_page_create',
      '#title' => $this->t('Create') . ' ' .t('User'),
      '#form' => $form,
      '#cache' => ['max-age' => 0]
    ];
  }

  public function edit_user($id) {
    $form = $this->formBuilder()->getForm('Drupal\mt_user\Form\EditForm', $id);
    return [
      '#theme' => 'theme_mt_user_page_edit',
      '#title' => $this->t('Edit') . ' ' . t('User'),
      '#form' => $form,
      '#cache' => ['max-age' => 0]
    ];
  }

  public function api_user($params = [], $json = true) {
    if (empty($params)) $params = \Drupal::request()->query->all();
    $class = new MTUserProvider();
    $register = $class->getAPIUser($params);
    if (!empty($register)) {
      $data['code'] = 200;
      $data['data'] = $register;
    } else {
      $data['code'] = 204;
    }
    if ($json) {
      return new JsonResponse($data);
    } else {
      return $data;
    }
  }

  public function get_export_user_list($objSheet, $params) {

    // Get data
    $registers = $this->api_user($params, false);

    $n = 1;
    $c = 0;
    $cell = ['A','B','C'];
    $nameCellHeader = ['ID', 'NOMBRE', 'ESTADO'];

    foreach ($cell as $key => $item) {
      $objSheet->SetCellValue($cell[$c].$n ,$nameCellHeader[$c]);
      $objSheet->getColumnDimension($cell[$c])->setWidth(20);
      $c++;
    }
    $objSheet->getColumnDimension($cell[0])->setWidth(6);
    $objSheet->getColumnDimension($cell[1])->setWidth(25);
    $objSheet->getColumnDimension($cell[2])->setWidth(14);

    // Header
    $phpColor = new \PHPExcel_Style_Color();
    $phpColor->setRGB('FFFFFF');
    $objSheet->getStyle('A1:C1')->getFont()->setColor($phpColor);
    $objSheet
    ->getStyle('A1:C1')
    ->getFill()
    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB('d62d2d');
    $objSheet->getRowDimension(1)->setRowHeight(18);
    $r = 2;
    if (!empty($registers['data'])) {
      foreach ($registers['data'] as $key => $val) {
        $status= !empty($val->status) ? 'Activado' : 'Desactivado';
        $objSheet->SetCellValue('A'.$r , $val->id);
        $objSheet->SetCellValue('B'.$r , $val->name);
        $objSheet->SetCellValue('C'.$r , $status);
        $r++;
      }
    }

    return $objSheet;
  }

  public function get_export_user_template($objSheet) {

    $n = 1;
    $c = 0;
    $cell = ['A','B','C'];
    $nameCellHeader = ['ID', 'NOMBRE', 'ESTADO'];

    foreach ($cell as $key => $item) {
      $objSheet->SetCellValue($cell[$c].$n ,$nameCellHeader[$c]);
      $objSheet->getColumnDimension($cell[$c])->setWidth(20);
      $c++;
    }
    $objSheet->getColumnDimension($cell[0])->setWidth(6);
    $objSheet->getColumnDimension($cell[1])->setWidth(25);
    $objSheet->getColumnDimension($cell[2])->setWidth(14);

    // Header
    $phpColor = new \PHPExcel_Style_Color();
    $phpColor->setRGB('FFFFFF');
    $objSheet->getStyle('A1:C1')->getFont()->setColor($phpColor);
    $objSheet
    ->getStyle('A1:C1')
    ->getFill()
    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB('d62d2d');
    $objSheet->getRowDimension(1)->setRowHeight(18);

    return $objSheet;
  }

}
