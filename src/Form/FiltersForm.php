<?php
/**
 * @file
 * Contains \Drupal\mt_user\Form\FiltersForm by Marco Torres marquillo01@gmail.com.
 */

namespace Drupal\mt_user\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\mt_user\MTUserProvider;

/**
 * Filters form.
 */
class FiltersForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mt_user_filters_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $filters = NULL) {

    $option_status = [
      1 => t('Yes'),
      0 => t('No')
    ];

    $form = [
      '#attributes' => [
        'onSubmit' => 'return sendForm()',
        'class' => ['container-inline']
      ],
      'user_filter_id' => [
        '#type' => 'textfield',
        '#title' => t('ID'),
        '#size' => 20,
        '#maxlength' => 6,
        '#default_value' => !empty($filters['id']) ? $filters['id'] : '',
        '#required' => FALSE
      ],
      'user_filter_name' => [
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#size' => 40,
        '#maxlength' => 60,
        '#default_value' => !empty($filters['name']) ? $filters['name'] : '',
        '#required' => FALSE
      ],
      'user_filter_active' => [
        '#type' => 'select',
        '#title' => t('Active'),
        '#empty_option' => t('All'),
        '#options' => $option_status,
        '#default_value' => isset($filters['status']) ? $filters['status'] : '',
        '#required' => FALSE
      ]
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => t('Filter'),
    ];

    // Link user export
    $url_export = Url::fromRoute('mt_user.admin.export');
    $link_export_options = [
      'attributes' => [
        'class' => ['button', 'btn', 'btn-primary', 'btn-warning', 'btn-mt-user-export'],
        'target' => '_blank',
      ]
    ];
    $url_export->setOptions($link_export_options);
    $link_export = Link::fromTextAndUrl(t('Export'), $url_export)->toString();

    // Link user import
    $url_import = Url::fromRoute('mt_user.admin.import');
    $link_import_options = [
      'attributes' => [
        'class' => ['button', 'btn', 'btn-primary', 'btn-warning', 'btn-mt-user-import'],
      ]
    ];
    $url_import->setOptions($link_import_options);
    $link_import = Link::fromTextAndUrl(t('Import'), $url_import)->toString();

    $suffix = $form['actions']['submit']['#suffix'];
    $form['actions']['submit']['#suffix'] = $suffix . $link_export . $link_import;

    if (!empty($filters)) {
      $form['reset'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#submit' => array('::resetForm'),
      );
    }

    $form['#attached']['library'][] = 'mt_user/mt-user';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $filters = [
      'id' => $values['user_filter_id'],
      'name' => $values['user_filter_name'],
      'status' => $values['user_filter_active'],
    ];
    $form_state->setRedirect('mt_user.admin.list', [], [
      'query' => [
        'search' => $filters
      ]
    ]);
  }

  /**
   * Resets the filter selections.
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $form_state->setRedirect('mt_user.admin.list');
  }

}
?>
