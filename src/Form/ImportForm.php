<?php
/**
 * @file
 * Contains \Drupal\mt_user\Form\ImportForm by Marco Torres marquillo01@gmail.com.
 */

namespace Drupal\mt_user\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\file\Entity\File;
use Drupal\Component\Utility\UrlHelper;
use Drupal\mt_user\MTUserProvider;

/**
 * Import form.
 */
class ImportForm extends FormBase {
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mt_user_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Link export template
    $url_export_template = Url::fromRoute('mt_user.admin.export_template');
    $link_export_template_options = [
      'attributes' => [
        'class' => ['button', 'btn', 'btn-primary', 'btn-success'],
        'target' => '_blank'
      ]
    ];
    $url_export_template->setOptions($link_export_template_options);
    $link_export_template = Link::fromTextAndUrl(t('Template'), $url_export_template)->toString();

    // Link return
    $url_return = Url::fromRoute('mt_user.admin.list');
    $link_return_options = [
      'attributes' => [
        'class' => ['button', 'btn', 'btn-primary', 'btn-info'],
        'target' => '_blank'
      ]
    ];
    $url_return->setOptions($link_return_options);
    $link_return = Link::fromTextAndUrl(t('Return'), $url_return)->toString();

    $form = [
      'mt_user_import' => [
        '#type' => 'details',
        '#title' => t('Import'),
        '#open' => TRUE,
        'mt_user_import_file' => [
          '#type' => 'managed_file',
          '#name' => 'mt_user_import_file',
          '#title' => $this->t('File'),
          '#size' => 20,
          '#description' => t('Extension: .csv'),
          '#upload_validators' => ['file_validate_extensions' => ['csv']],
          '#upload_location' => 'public://users/imports/',
          '#required' => TRUE,
        ]
      ],
      'submit' => [
        '#type' => 'submit',
        '#value' => t('Import'),
        '#suffix' => $link_export_template . $link_return,
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {   
    if ($form_state->getValue('mt_user_import_file') == NULL) {
      $form_state->setErrorByName('mt_user_import_file', $this->t('File'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $file = \Drupal::entityTypeManager()->getStorage('file')
      ->load($form_state->getValue('mt_user_import_file')[0]);

    if (!empty($file)) {

      // Get data
      $class = new MTUserProvider();
      $user_registers = $class->getAPIUser(['all' => 1]);
      $users = [];
      if (!empty($user_registers))
        foreach ($user_registers as $item) $users[$item->name] = $item->name;

      $url = $file->url();
      $file_data = file_get_contents(str_replace(' ', '%20', $url));

      if (!empty($file_data)) {

        $results = explode("\n", $file_data);
        if (!empty($results[0])) unset($results[0]);
        $end = end(array_keys($results));
        if (empty($results[$end])) unset($results[$end]);

        if (!empty($results)) {

          // Variables
          $i = $n = 0;
          $message_error = '';
          foreach ($results as $row) {

            // Vars
            $j = $i+1;
            $valid = TRUE;
            $record = explode(';', utf8_encode($row));
            $record = !empty($record[1]) ? $record : explode(',', $row);
            $record_trim = $record;
            foreach ($record_trim as $key => $value) $record[$key] = trim($value);

            if ($record) {
              if (!empty($record[1])) {

                // Get data
                $values = [
                  'name' => trim($record[1]),
                  'status' => $record[23]
                ];

                // Valid
                if (empty($users[$record[1]])) {
                  if ($valid && in_array($values['status'], ['Activado', 'Desactivado']))
                    $values['status'] = ($values['status'] == 'Activado') ? TRUE : FALSE;
                  $result = $class->createUser($values);
                  if ($result) {
                    $n++;
                  }
                  else {
                    drupal_set_message(t('Register').' '.$j.', '.t('error in data'), 'error');
                  }
                }
                else {
                  drupal_set_message(t('Register').' '.$j.', '.t('user exists'), 'error');
                }
              }
              else {
                drupal_set_message(t('Register').' '.$j.', '.t('is invalid'), 'error');
              }
              $i++;
            }
          }
          if ($n) drupal_set_message($n.' '.t('imported records'), 'status');
        }

      }
    }
  }

}
