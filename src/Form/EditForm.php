<?php
/**
 * @file
 * Contains \Drupal\mt_user\Form\EditForm by Marco Torres marquillo01@gmail.com.
 */

namespace Drupal\mt_user\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\mt_user\MTUserProvider;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Edit form.
 */
class EditForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mt_user_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {

    if ($id) {

      $class = new MTUserProvider();
      $data = $class->getUser($id);
      if (!empty($data)) {

        $form = [
          '#attributes' => [
            'onSubmit' => 'return sendForm()'
          ],
          'user_edit' => [
            '#type' => 'details',
            '#title' => t('Edit User'),
            '#open' => TRUE,
            'user_id' => [
              '#type' => 'hidden',
              '#value' => $id,
              '#required' => TRUE
            ],
            'user_name' => [
              '#type' => 'textfield',
              '#title' => t('Name'),
              '#size' => 20,
              '#minlength' => 5,
              '#maxlength' => 60,
              '#default_value' => $data->name,
              '#required' => TRUE
            ],
            'user_active' => [
              '#type' => 'checkbox',
              '#title' => t('Active'),
              '#default_value' => $data->status
            ]
          ]
        ];

        // Link return
        $url_return = Url::fromRoute('mt_user.admin.list');
        $link_return_options = [
          'attributes' => [
            'class' => ['button', 'btn', 'btn-primary', 'btn-info'],
            'target' => '_blank'
          ]
        ];
        $url_return->setOptions($link_return_options);
        $link_return = Link::fromTextAndUrl(t('Return'), $url_return)->toString();

        $form['submit'] = [
          '#type' => 'submit',
          '#value' => t('Update User'),
          '#suffix' => $link_return,
        ];

        $url = new Url('mt_user.admin.drop', ['id' => $id]);

        if ($this->getRequest()->query->has('destination')) {
          $url->setOption('query', $this->getDestinationArray());
        }

        $form['actions']['delete'] = array(
          '#type' => 'link',
          '#title' => $this->t('Delete'),
          '#url' => $url,
          '#attributes' => array(
            'class' => array('btn', 'btn-danger'),
          ),
        );

        $form['#attached']['library'][] = 'mt_user/mt-user';

        return $form;

      }
    }

    drupal_set_message(t('Registration with id %id does not exist.', ['%id' => $id]), 'error');
    return new RedirectResponse(\Drupal::url('mt_user.admin.list'));
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $id = $values['user_id'];
    $class = new MTUserProvider();
    $data = $class->processDataUser($values);
    if ($class->editUser($id, $data)) {
      drupal_set_message(t('Registration with <strong>%id</strong> update successfully.', ['%id' => $id]), 'status');
      $form_state->setRedirect('mt_user.admin.list');
    }
  }
}
?>