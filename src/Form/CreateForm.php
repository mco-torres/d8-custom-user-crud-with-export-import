<?php
/**
 * @file
 * Contains \Drupal\mt_user\Form\CreateForm by Marco Torres marquillo01@gmail.com.
 */

namespace Drupal\mt_user\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mt_user\MTUserProvider;
use Symfony\Component\HttpFoundation\UserResponse;

/**
 * Create form.
 */
class CreateForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mt_user_create_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = [
      '#attributes' => [
        'onSubmit' => 'return sendForm()'
      ],
      'user_create' => [
        '#type' => 'details',
        '#title' => t('Create User'),
        '#open' => TRUE,
        'user_name' => [
          '#type' => 'textfield',
          '#title' => t('Name'),
          '#size' => 20,
          '#minlength' => 5,
          '#maxlength' => 60,
          '#required' => TRUE
        ],
        'user_active' => [
          '#type' => 'checkbox',
          '#title' => t('Active'),
          '#default_value' => 1
        ]
      ]
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Create User'),
    ];

    $form['#attached']['library'][] = 'mt_user/mt-user';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $class = new MTUserProvider();
    $data = $class->processDataUser($values);
    if ($class->createUser($data)) {
      $id = $class->getLastUser();
      drupal_set_message(t('Registration with id  <strong>%id</strong> created successfully.', ['%id' => $id]), 'status');
      $form_state->setRedirect('mt_user.admin.list');
    }
  }

}
