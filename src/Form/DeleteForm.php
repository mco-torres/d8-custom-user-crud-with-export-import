<?php
/**
 * @file
 * Contains \Drupal\mt_user\Form\DeleteForm by Marco Torres marquillo01@gmail.com.
 */

namespace Drupal\mt_user\Form;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\mt_user\MTUserProvider;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Delete form.
 */
class DeleteForm extends ConfirmFormBase {

  /**
   * The ID of the item to delete.
   *
   * @var string
   */
  protected $id;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mt_user_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete the custom User %id?', array('%id' => $this->id));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl($id = NULL) {
    $id = $this->id;
    if ($this->getRequest()->query->has('destination')) {
      $url->setOption('query', $this->getDestinationArray());
    }
    else {
      $url = new Url('mt_user.admin.edit', array(
        'id' => $id,
      ));
    }
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return t('This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return t('Cancel');
  }

  /**
   * {@inheritdoc}
   *
   * @param int $id
   *   (optional) The ID of the item to be deleted.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $this->id = $id;
    $class = new MTUserProvider();
    $data = $class->getUser($id);
    if (empty($data)) {
      drupal_set_message(t('Registration with id %id does not exist.', ['%id' => $id]), 'error');
      return new RedirectResponse(\Drupal::url('mt_user.admin.list'));
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $id = $this->id;
    $class = new MTUserProvider();
    if ($class->deleteUser($id)) {
      drupal_set_message(t('Registration with id <strong>%id</strong> deleted successfully.', ['%id' => $id]), 'status');
      $form_state->setRedirect('mt_user.admin.list');
    }
  }

}
